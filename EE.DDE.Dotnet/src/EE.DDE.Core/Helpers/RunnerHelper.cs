﻿using System;
using EE.DDE.Core.Calculators;
using EE.DDE.Core.Calculators.Decorators;

namespace EE.DDE.Core.Helpers
{
    public static class RunnerHelper
    {
        public static IRunner WithTimer(this IRunner runner)
        {
            return new TimerRunnerDecorator(runner);
        }

        public static IRunner WithStandartComprarison(this IRunner runner, Func<double, double, double> standart, bool withChanges)
        {
            return new MaxErrorRunnerDecorator(runner, standart, withChanges);
        }

        public static IRunner WithPrintStandart(this IRunner runner, Func<double, double, double> standart)
        {
            return new PrintStandartRunnerDecorator(runner, standart);
        }

        public static IRunner WithDisplay(this IRunner runner)
        {
            return new DisplayRunnerDecorator(runner);
        }
    }
}