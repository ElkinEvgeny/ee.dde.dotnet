﻿using System;

namespace EE.DDE.Core.Helpers
{
    public static class MatrixHelper
    {
        public static void Print(this double[,] matrix)
        {
            for (var j = 0; j < matrix.GetLength(1); j++)
            {
                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    Console.Write($"{matrix[i, j]:F2} ");
                }
                Console.WriteLine();
            }
        }
    }
}
