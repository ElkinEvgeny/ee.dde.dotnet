﻿using System;
using EE.DDE.Core.Models;

namespace EE.DDE.Core.Calculators.Decorators
{
    public class MaxErrorRunnerDecorator : RunnerDecoratorBase
    {
        private readonly Func<double, double, double> _standart;
        private readonly bool _withChanges;
        public MaxErrorRunnerDecorator(IRunner runner, Func<double, double, double> standart, bool withChanges) : base(runner)
        {
            _standart = standart;
            _withChanges = withChanges;
        }

        public override void Run()
        {
            Runner.Run();
            var net = Method.Net;
            var maxError = double.MinValue;
            for (var i = 1; i <= net.Width; i++)
            {
                for (var j = 1; j <= net.Height; j++)
                {
                    var error = Math.Abs(net[i, j] - _standart(net.ImageX(i), net.ImageT(j)));
                    if (maxError < error)
                    {
                        maxError = error;
                    }
                    if (_withChanges)
                    {
                        net[i, j] = error;
                    }
                }
            }
            Console.WriteLine("{0:f2} ", maxError);
            //Console.WriteLine("MaxErros: {0}", maxError);
        }
    }
}