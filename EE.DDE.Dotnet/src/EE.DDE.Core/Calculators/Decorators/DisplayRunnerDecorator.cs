﻿using System;

namespace EE.DDE.Core.Calculators.Decorators
{
    public class DisplayRunnerDecorator : RunnerDecoratorBase
    {
        public DisplayRunnerDecorator(IRunner runner) : base(runner)
        { }

        public override void Run()
        {
            Runner.Run();
            var net = Method.Net;
            for (var i = 1; i <= net.Height; i++)
            {
                for (var j = 1; j <= net.Width; j++)
                {
                    Console.Write("{0:f2} ", net[j,i]);
                }
                Console.WriteLine();
            }
        }
    }
}