﻿using System;
using System.Diagnostics;

namespace EE.DDE.Core.Calculators.Decorators
{
    public class TimerRunnerDecorator : RunnerDecoratorBase
    {
        public TimerRunnerDecorator(IRunner runner) : base(runner)
        { }

        public override void Run()
        {
            var watcher = Stopwatch.StartNew();
            watcher.Start();
            Runner.Run();
            watcher.Stop();
            Console.WriteLine($"ComputedComplete: {watcher.ElapsedMilliseconds}ms");
        }
    }
}
