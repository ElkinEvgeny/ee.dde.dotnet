﻿using System;
using EE.DDE.Core.Models;

namespace EE.DDE.Core.Calculators.Decorators
{
    public class PrintStandartRunnerDecorator : RunnerDecoratorBase
    {
        private readonly Func<double, double, double> _standart;
        public PrintStandartRunnerDecorator(IRunner runner, Func<double, double, double> standart) : base(runner)
        {
            _standart = standart;
        }

        public override void Run()
        {
            Runner.Run();
            var net = Method.Net;
            for (var i = 1; i < net.Width; i++)
            {
                for (var j = 1; j < net.Height; j++)
                {
                    Console.Write("{0:f2} ", _standart(net.ImageX(i), net.ImageT(j)));
                }
                Console.WriteLine();
            }
        }
    }
}