﻿using EE.DDE.Core.Methods;

namespace EE.DDE.Core.Calculators.Decorators
{
    public abstract class RunnerDecoratorBase : IRunner
    {
        protected readonly IRunner Runner;

        protected RunnerDecoratorBase(IRunner runner)
        {
            Runner = runner;
        }

        public IMethod Method => Runner.Method;
        public abstract void Run();
    }
}