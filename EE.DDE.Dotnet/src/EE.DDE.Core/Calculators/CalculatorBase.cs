﻿using System;
using EE.DDE.Core.Configs;
using EE.DDE.Core.Interfaces;

namespace EE.DDE.Core.Calculators
{
    public abstract class CalculatorBase : ICalculator
    {
        protected readonly double[,] Values;
        protected readonly Func<double [,], int, int, double> MethodFunction;
        public CalculatorConfig Config { get; }

        protected CalculatorBase(IMethodFactory mentod, CalculatorConfig config, double[,] values, Func<double, double, double, double> u)
        {
            Config = config;
            Values = values;
            MethodFunction = mentod.GetMethodFunction(config, u);
        }

        public abstract double[,] Calculate();

        /// <summary>
        /// Метод для тестирования корректности итераций
        /// </summary>
        /// <param name="iteration"></param>
        public virtual void HandleComplete(int iteration)
        { }
    }
}
