﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EE.DDE.Core.Configs;
using EE.DDE.Core.Interfaces;

namespace EE.DDE.Core.Calculators
{
    public class DiagonalParallelWithLinqCalculator : CalculatorBase
    {
        public DiagonalParallelWithLinqCalculator(IMethodFactory methodFactory, CalculatorConfig config, double[,] values,
            Func<double, double, double, double> u)
            : base(methodFactory, config, values, u)
        {
        }

        public override double[,] Calculate()
        {
            var list = new List<Tuple<int, int>> {Tuple.Create(1, Config.Tau + 1)};
            while (list.Any())
            {
                Parallel.ForEach(list, x => { Values[x.Item1, x.Item2] = MethodFunction(Values, x.Item1, x.Item2); });

                list = list.SelectMany(x => new List<Tuple<int, int>>
                    {
                        Tuple.Create(x.Item1 + 1, x.Item2),
                        Tuple.Create(x.Item1, x.Item2 + 1),
                    })
                    .Distinct()
                    .Where(x => x.Item1 <= Config.XSize && x.Item2 <= Config.YSize)
                    .ToList();
            }
            return Values;
        }


    }
}