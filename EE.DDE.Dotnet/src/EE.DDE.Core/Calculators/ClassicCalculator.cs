﻿using System;
using EE.DDE.Core.Configs;
using EE.DDE.Core.Interfaces;

namespace EE.DDE.Core.Calculators
{
    public class ClassicCalculator : CalculatorBase
    {
        public ClassicCalculator(IMethodFactory methodFactory, CalculatorConfig config, double[,] values, Func<double, double, double, double> u) 
            : base(methodFactory, config, values, u)
        { }

        public override double[,] Calculate()
        {
            for (var j = Config.Tau + 1; j <= Config.YSize; j++)
            {
                for (var i = 1; i <= Config.XSize; i++)
                {
                    Values[i, j] = MethodFunction(Values, i, j);
                }
            }
            return Values;
        }
    }
}
