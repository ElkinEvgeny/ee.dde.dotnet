﻿using System;
using System.Threading.Tasks;
using EE.DDE.Core.Methods;

namespace EE.DDE.Core.Calculators
{
    public class ClassicRunner : IRunner
    {
        public IMethod Method { get; }

        public ClassicRunner(IMethod method)
        {
            Method = method;
        }

        public void Run()
        {
            for (var i = 1; i <= Method.Net.Width; i++)
            {
                for (var j = 1; j <= Method.Net.Height; j++)
                {
                    Method.Step(i, j);
                }
            }
        }
    }

    public class ParallelRunner : IRunner
    {
        public ParallelRunner(IMethod method)
        {
            Method = method;
        }

        public IMethod Method { get; }

        public void Run()
        {
            var options = new ParallelOptions
            {
                MaxDegreeOfParallelism = 8
            };
            for (var i = 1; i <= Method.Net.Width; i++)
            {
                Parallel.For(1, i + 1, options, j => Method.Step(i - j + 1, j));
            }
            for (var i = 2; i <= Method.Net.Width; i++)

                Parallel.For(i, Method.Net.Width + 1, options, j => Method.Step(Method.Net.Width - j + i, j));
        }
    }
}