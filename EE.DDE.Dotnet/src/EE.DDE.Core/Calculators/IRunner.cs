﻿using EE.DDE.Core.Methods;

namespace EE.DDE.Core.Calculators
{
    public interface IRunner
    {
        IMethod Method { get; }
        void Run();
    }
}