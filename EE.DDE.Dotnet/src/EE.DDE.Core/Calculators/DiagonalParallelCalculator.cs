﻿using System;
using System.Threading.Tasks;
using EE.DDE.Core.Configs;
using EE.DDE.Core.Interfaces;

namespace EE.DDE.Core.Calculators
{
    public class DiagonalParallelCalculator : CalculatorBase
    {
        public DiagonalParallelCalculator(IMethodFactory methodFactory, CalculatorConfig config, double[,] values,
            Func<double, double, double, double> u)
            : base(methodFactory, config, values, u)
        { }

        public override double[,] Calculate()
        {
            var iterationCount = Config.XSize + Config.YSize - Config.Tau - 1;
            var barrier = Math.Min(Config.XSize, Config.YSize - Config.Tau - 1);
            //Неработает!
            for (var i = 0; i <= iterationCount; i++)
            {
                if (i <= barrier)
                {
                    var x = i + 1;
                    Parallel.For(1, x, j =>
                    {
                        Values[x - j, j + Config.Tau] = MethodFunction(Values,x - j, j + Config.Tau);
                    });
                }
                else
                {
                    var y = i - barrier + 1;
                    Parallel.For(y, Config.YSize - Config.Tau + 1, j =>
                    {
                        Values[barrier - (j - y), j + Config.Tau] = MethodFunction(Values, barrier - (j - y), j + Config.Tau);
                    });
                }
                HandleComplete(i);
            }
            return Values;
        }
    }
}