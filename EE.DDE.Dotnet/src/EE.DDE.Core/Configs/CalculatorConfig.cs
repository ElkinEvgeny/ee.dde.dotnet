namespace EE.DDE.Core.Configs
{
    public class CalculatorConfig
    {
        public int XSize { get; }
        public int YSize { get; }
        public int Tau { get; }
        public double H { get; }
        public double D { get; } 

        public CalculatorConfig(int xSize, int ySize, int tau, double h, double d)
        {
            Tau = tau;
            H = h;
            D = d;

            XSize = xSize;
            YSize = ySize;
        }
    }
}