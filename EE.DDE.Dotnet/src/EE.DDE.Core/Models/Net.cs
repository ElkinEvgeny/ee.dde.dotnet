﻿using System;

namespace EE.DDE.Core.Models
{
    public class Net
    {
        public double X { get; }
        public double T { get; }
        public double Tau { get; }
        public double D { get; }
        public double H { get; }
        public int Width { get; }
        public int Height { get; }
        public int Offset { get; }
        private readonly double[,] _values;

        public Net(double x, double t, double tau, double d, double h)
        {
            X = x;
            T = t;
            Tau = tau;
            D = d;
            H = h;
            Offset = (int)Math.Floor(Tau/d) + 1;
            Width = (int)Math.Floor(X/h);
            Height = (int) Math.Floor(T/d);
            _values = new double[Width + 1, Height + Offset];
        }

        public double this[int i, int j]
        {
            get { return _values[i, j + Offset - 1]; }
            set { _values[i, j + Offset - 1] = value; }
        }
    }
}
