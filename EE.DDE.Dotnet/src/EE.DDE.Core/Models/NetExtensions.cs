﻿using System;

namespace EE.DDE.Core.Models
{
    public static class NetExtensions
    {
        public static double ImageX(this Net net, int i)
        {
            return i*net.H;
        }

        public static double ImageT(this Net net, int j)
        {
            return j*net.D;
        }
        
        public static Net SetBorderCondition(this Net net, Func<double, double> borderCondition)
        {
            for (int i = net.Height; i > -net.Offset; i--)
            {
                net[0, i] = borderCondition(net.ImageT(i));
            }
            return net;
        }

        public static Net SetDelayFunction(this Net net, Func<double, double, double> delayFunction)
        {
            for (var i = 1; i <= net.Width ; i++)
            {
                for (var j = 0; j > -net.Offset; j--)
                {
                    net[i,j] = delayFunction(net.ImageX(i), net.ImageT(j));
                }
            }
            return net;
        }
    }
}