﻿using System;
using EE.DDE.Core.Models;

namespace EE.DDE.Core.Methods
{
    public class ConstantRightAngleMethod : IMethod
    {
        public Net Net { get; }
        private readonly int _gammaOffset;
        private readonly Func<double, double, double, double> _u;

        public ConstantRightAngleMethod(Net net, Func<double, double, double, double> u, double gamma)
        {
            Net = net;
            _u = u;
            if (gamma > net.Tau)
                throw new Exception("Offset greater then delay function");
            _gammaOffset = (int) Math.Floor(gamma/Net.D);
        }
        
        public void Step(int i, int j)
        {
            Net[i,j] = (Net.H * Net[i - 1, j] + Net.D * Net[i, j - 1]
                         + _u(Net.ImageX(i), Net.ImageT(j), Net[i, j - _gammaOffset]) * Net.H * Net.D) / (Net.H + Net.D);
        }
    }
}