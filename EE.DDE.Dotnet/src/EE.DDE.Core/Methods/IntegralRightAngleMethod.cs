﻿using System;
using EE.DDE.Core.Models;

namespace EE.DDE.Core.Methods
{
    public class IntegralRightAngleMethod : IMethod
    {
        public Net Net { get; }
        private readonly Func<double, double, double, double> _u;
        private readonly int _minOffset;
        private readonly int _maxOffset;

        public IntegralRightAngleMethod(Net net, Func<double, double, double, double> u, double minOffset, double maxOffset)
        {
            Net = net;
            _minOffset = (int)Math.Floor(minOffset/net.D);
            _maxOffset = (int)Math.Floor(maxOffset/net.D);
            _u = u;
        }

        public void Step(int i, int j)
        {
            var delayedValue = 0.0;
            for (int k = j - _minOffset; k < j - _maxOffset; k++)
            {
                delayedValue += (Net[i, k] + Net[i, k + 1])/2 * Net.D;
            }
            Net[i, j] = (Net.H * Net[i - 1, j] + Net.D * Net[i, j - 1]
                         + _u(Net.ImageX(i), Net.ImageT(j), delayedValue) * Net.H * Net.D) / (Net.H + Net.D + Net.D * Net.H);
        }
    }
}