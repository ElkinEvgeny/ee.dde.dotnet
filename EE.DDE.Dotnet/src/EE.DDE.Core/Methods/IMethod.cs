﻿using EE.DDE.Core.Models;

namespace EE.DDE.Core.Methods
{
    public interface IMethod
    {
        Net Net { get; }
        void Step(int i, int j);
    }
}