﻿using System;
using EE.DDE.Core.Models;

namespace EE.DDE.Core.Methods
{
    public class VariableRightAngleMethod : IMethod
    {
        public Net Net { get; }
        private readonly Func<double, double, double, double> _u;
        private readonly int _gammaOffset;

        public VariableRightAngleMethod(Net net, Func<double, double, double, double> u, double gammaOffset = 0)
        {
            Net = net;
            _u = u;
            if (gammaOffset > Net.Tau)
                throw new Exception("Offset greater then delay function");
            _gammaOffset = (int)Math.Floor(gammaOffset/Net.D);
        }

        public void Step(int i, int j)
        {
            var delayedValue = j % 2 == 0
                ? Net[i, j / 2 - _gammaOffset]
                : (Net[i, (int)Math.Ceiling(j / 2.0) - _gammaOffset] + Net[i, (int)Math.Floor(j / 2.0) - _gammaOffset]) / 2;
            Net[i, j] = (Net.H * Net[i - 1, j] + Net.D * Net[i, j - 1]
                          + _u(Net.ImageX(i), Net.ImageT(j), delayedValue) * Net.H * Net.D) / (Net.H + Net.D);
        }

        //public void Step(int i, int j)
        //{
        //    var delayedValue = j % 2 == 0
        //        ? Net[i, j / 2]
        //        : (Net[i, (int)Math.Ceiling(j / 2.0)] + Net[i, (int)Math.Floor(j / 2.0)]) / 2;

        //    Net[i, j] = (Net.H * Net[i - 1, j] + Net.D * Net[i, j - 1] + _u(Net.ImageX(i), Net.ImageT(j), 0)*Net.H*Net.D)
        //                   / (Net.H + Net.D - (Net.D * Net.H / delayedValue));
        //}
    }
}