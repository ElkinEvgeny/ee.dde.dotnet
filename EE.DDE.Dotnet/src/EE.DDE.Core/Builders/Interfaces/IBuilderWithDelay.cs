﻿using System;

namespace EE.DDE.Core.Builders.Interfaces
{
    public interface IBuilderWithDelay
    {
        IBuilderWithBorderConditions SetBorderConditions(Func<double, double> borderConditions);
    }
}