﻿using System;

namespace EE.DDE.Core.Builders.Interfaces
{
    public interface IBuilder
    {
        IBuilderWithFunction SetFunction(Func<double, double, double, double> uFunc);
    }
}