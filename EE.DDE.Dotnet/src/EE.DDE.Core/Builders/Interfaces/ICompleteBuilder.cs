﻿using System;
using System.Collections.Generic;
using EE.DDE.Core.Interfaces;

namespace EE.DDE.Core.Builders.Interfaces
{
    public interface ICompleteBuilder
    {
        IBuilderWithStandart SetStandart(Func<double, double, double> standart);
        ICompleteBuilder AddKeys(double h, double d);
        IEnumerable<ICalculator> Build();
    }
}