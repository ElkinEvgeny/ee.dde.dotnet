﻿namespace EE.DDE.Core.Builders.Interfaces
{
    public interface IBuilderWithType
    {
        ICompleteBuilder AddKeys(double h, double d);
    }
}