﻿using System.Collections.Generic;
using EE.DDE.Core.Interfaces;

namespace EE.DDE.Core.Builders.Interfaces
{
    public interface IBuilderWithStandart
    {
        IEnumerable<ICalculator> Build();
    }
}