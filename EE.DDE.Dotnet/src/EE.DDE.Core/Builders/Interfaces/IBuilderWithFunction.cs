﻿using System;

namespace EE.DDE.Core.Builders.Interfaces
{
    public interface IBuilderWithFunction
    {
        IBuilderWithDelay SetDelayFunction(Func<double, double, double> delayFunction);
    }
}