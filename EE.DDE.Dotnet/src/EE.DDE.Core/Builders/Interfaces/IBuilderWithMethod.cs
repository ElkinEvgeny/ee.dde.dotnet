﻿using EE.DDE.Core.Enums;

namespace EE.DDE.Core.Builders.Interfaces
{
    public interface IBuilderWithMethod
    {
        IBuilderWithType SetCalculatorType(CalculatorTypes type);
    }
}