﻿using EE.DDE.Core.Enums;

namespace EE.DDE.Core.Builders.Interfaces
{
    public interface IBuilderWithBorderConditions
    {
        IBuilderWithMethod SetMethod(MethodTypes type);
    }
}