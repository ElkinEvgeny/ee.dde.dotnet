﻿//using System;
//using System.Collections.Generic;
//using EE.DDE.Core.Builders.Interfaces;
//using EE.DDE.Core.Configs;
//using EE.DDE.Core.Enums;
//using EE.DDE.Core.Helpers;
//using EE.DDE.Core.Interfaces;

//namespace EE.DDE.Core.Builders
//{
//    public static class CalcaulatorBuilder
//    {
//        public static IBuilder CreateBuilder(double x, double t, double tau)
//        {
//            return new CalculatorBuilderBase(x, t, tau);
//        }

//        private class CalculatorBuilderBase : IBuilder, IBuilderWithFunction, IBuilderWithDelay,
//            IBuilderWithBorderConditions, IBuilderWithType, ICompleteBuilder, IBuilderWithStandart, IBuilderWithMethod
//        {
//            private readonly double _x;
//            private readonly double _t;
//            private readonly double _tau;

//            private readonly List<Tuple<double, double>> _stepConfigs = new List<Tuple<double, double>>();
//            private Func<double, double, double, double> _u;
//            private Func<double, double, double> _delayFunction;
//            private Func<double, double> _borderConditions;
//            private Func<double, double, double> _standart;
//            private CalculatorTypes _calculatorType;
//            private MethodTypes _methodType;

//            /// <summary>
//            /// Конструктор по умолчанию получает параметры задачи
//            /// </summary>
//            /// <param name="x"></param>
//            /// <param name="t"></param>
//            /// <param name="tau"></param>
//            public CalculatorBuilderBase(double x, double t, double tau)
//            {
//                _x = x;
//                _t = t;
//                _tau = tau;
//            }

//            /// <summary>
//            /// Задает функцию правой части ДУ
//            /// </summary>
//            /// <param name="uFunc"></param>
//            /// <returns></returns>
//            public IBuilderWithFunction SetFunction(Func<double, double, double, double> uFunc)
//            {
//                _u = uFunc;
//                return this;
//            }

//            /// <summary>
//            /// Задает функцию запаздывания
//            /// </summary>
//            /// <param name="delayFunction"></param>
//            /// <returns></returns>
//            public IBuilderWithDelay SetDelayFunction(Func<double, double, double> delayFunction)
//            {
//                _delayFunction = delayFunction;
//                return this;
//            }

//            /// <summary>
//            /// Задает граничные условия u(0,t);
//            /// </summary>
//            /// <param name="borderConditions"></param>
//            /// <returns></returns>
//            public IBuilderWithBorderConditions SetBorderConditions(Func<double, double> borderConditions)
//            {
//                _borderConditions = borderConditions;
//                return this;
//            }


//            /// <summary>
//            /// Задает метод которым будет вычислятся уравнение
//            /// </summary>
//            /// <param name="type">Тип метода</param>
//            /// <returns></returns>
//            public IBuilderWithMethod SetMethod(MethodTypes type)
//            {
//                _methodType = type;
//                return this;
//            }

//            /// <summary>
//            /// Задает тип вычислителя
//            /// </summary>
//            /// <param name="type">тип вычислителя</param>
//            /// <returns></returns>
//            public IBuilderWithType SetCalculatorType(CalculatorTypes type)
//            {
//                _calculatorType = type;
//                return this;
//            }


//            /// <summary>
//            /// Добавить набор параметро для вычисления
//            /// </summary>
//            /// <param name="h">Размер шага по X</param>
//            /// <param name="d">Размер шага по Y</param>
//            /// <returns>Возвращает экзмпляр строителя</returns>
//            public ICompleteBuilder AddKeys(double h, double d)
//            {
//                _stepConfigs.Add(Tuple.Create(h, d));
//                return this;
//            }

//            /// <summary>
//            /// Добавить эталонную функцию для вычисления погрешности
//            /// </summary>
//            /// <param name="standart">Функция эталон</param>
//            /// <returns>Возвращает экзмпляр строителя</returns>
//            public IBuilderWithStandart SetStandart(Func<double, double, double> standart)
//            {
//                _standart = standart;
//                return this;
//            }

//            public IEnumerable<ICalculator> Build()
//            {
//                foreach (var options in _stepConfigs)
//                {
//                    var config = GetConfig(options);
//                    var initValues = GetInitValues(config);
//                    var result = CalculatorHelper.CreateCalculator(_calculatorType, _methodType, config, initValues, _u)
//                        .WithTimer();

//                    yield return _standart == null
//                        ? result
//                        : result.WithStandartComprarison(_standart);
//                }
//            }

//            private CalculatorConfig GetConfig(Tuple<double, double> options)
//            {
//                var xSize = (int) Math.Floor(_x/options.Item1);
//                var ySize = (int) Math.Floor((_t + _tau)/options.Item2);
//                var delay = (int) Math.Floor(_tau/options.Item2);
//                return new CalculatorConfig(xSize, ySize, delay, options.Item1, options.Item2);
//            }

//            private double[,] GetInitValues(CalculatorConfig config)
//            {
//                var result = new double[config.XSize + 1, config.YSize + 1];

//                //Инициализаця граничного условия
//                for (var j = 0; j <= config.YSize; j++)
//                {
//                    var value = (j - config.Tau)*config.D;
//                    result[0, j] = _borderConditions(value);
//                }

//                //Инициализация функции запаздывания
//                for (var j = 0; j <= config.Tau; j++)
//                {
//                    for (var i = 1; i <= config.XSize; i++)
//                    {
//                        var computedX = i*config.H;
//                        var computedY = (j - config.Tau)*config.D;
//                        result[i, j] = _delayFunction(computedX, computedY);
//                    }
//                }

//                return result;
//            }
//        }
//    }
//}