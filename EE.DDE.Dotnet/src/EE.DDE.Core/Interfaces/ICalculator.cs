﻿using EE.DDE.Core.Configs;

namespace EE.DDE.Core.Interfaces
{
    public interface ICalculator
    {
        CalculatorConfig Config { get; }
        double[,] Calculate();
    }
}
