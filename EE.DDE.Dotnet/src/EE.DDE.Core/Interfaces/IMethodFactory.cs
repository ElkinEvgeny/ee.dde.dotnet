﻿using System;
using EE.DDE.Core.Configs;

namespace EE.DDE.Core.Interfaces
{
    public interface IMethodFactory
    {
        Func<double[,], int, int, double> GetMethodFunction(
            CalculatorConfig config,
            Func<double, double, double, double> u);
    }
}