﻿namespace EE.DDE.Core.Enums
{
    public enum MethodTypes
    {
        /// <summary>
        /// Правый уголок с постоянным запаздыванием
        /// </summary>
        RightAngleConstant,

        /// <summary>
        /// Правый уголок с переменным запаздыванием
        /// </summary>
        RightAngleVariable
    }
}