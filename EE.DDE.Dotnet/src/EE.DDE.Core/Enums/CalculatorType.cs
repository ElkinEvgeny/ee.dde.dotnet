﻿namespace EE.DDE.Core.Enums
{
    public enum CalculatorTypes
    {
        /// <summary>
        /// Классический последовательный вычислитель
        /// </summary>
        Classic,

        /// <summary>
        /// Вычислитель с парралельным вычислением по диагонали
        /// </summary>
        DiagonalParallel,

        /// <summary>
        /// Вычислитель с парралельным вычислением по диагонали, с использованием linq
        /// </summary>
        DiagonalParallelWithLinq
    }
}
