﻿using System;
using EE.DDE.Core.Calculators;
using EE.DDE.Core.Helpers;
using EE.DDE.Core.Methods;
using EE.DDE.Core.Models;

namespace EE.DDE.SimpleRunner
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //var stepsForConstnant = new[] {0.1, 0.05, 0.025, 0.0125, 0.00625, 0.003125};

            //foreach (var i in stepsForConstnant)
            //{;
            //    //foreach (var j in stepsForConstnant)
            //    {

            //        Console.Write("({0}:{1})", i, i);
            //        CalculateExampleOne(i, i);
            //        Console.WriteLine();
            //    }
            //}

            for (var i = 1; i < 10; i++)
            {
                CalculateExampleTwo(Math.Pow(2, -i + 1), Math.Pow(2, -i + 1));
            }


            //CalculateExampleTwo1(0.05, 0.05);
            //CalculateExampleTwo1(0.05, 0.025);
            //CalculateExampleTwo1(0.05, 0.0125);
            //CalculateExampleTwo1(0.025, 0.0125);
            //CalculateExampleTwo1(0.0125, 0.0125);
            //CalculateExampleTwo1(0.00625, 0.00625);

            //CalculateExampleThree(2, Math.PI / 2);
            //CalculateExampleThree(1, Math.PI / 4);
            //CalculateExampleThree(0.5, Math.PI / 8);
            //CalculateExampleThree(0.25, Math.PI / 16);
            //CalculateExampleThree(0.125, Math.PI / 32);
            //CalculateExampleThree(0.0625, Math.PI / 64);
            //CalculateExampleThree(0.03125, Math.PI / 128);
            //CalculateExampleThree(0.015625, Math.PI / 256);

            Console.ReadLine();
        }

        public static void CalculateExampleOne(double d, double h)
        {
            var net = new Net(4, 4, 2, d, h)
                .SetDelayFunction((x, t) => t*Math.Sin(Math.PI*x))
                .SetBorderCondition(t => 0);

            var method = new ConstantRightAngleMethod(net, (x, t, tauU) => Math.PI * t * Math.Cos(Math.PI * x) + (3 - t) * Math.Sin(Math.PI * x) + tauU, 2);
            var runner = new ParallelRunner(method)
                .WithTimer()
                .WithStandartComprarison((x, t) => t*Math.Sin(Math.PI*x), true);
                //.WithDisplay();
            runner.Run();
        }

        public static void CalculateExampleTwo(double h, double d)
        {
            var net = new Net(1, 2, 0.5, d, h)
                .SetDelayFunction((x, t) => Math.Pow(x,3) + Math.Pow(t, 3) + 3 * Math.Pow(x, 2) + 3 * Math.Pow(t, 2))
                .SetBorderCondition(t => 3 * t * t + 6 * t);

            var method = new VariableRightAngleMethod(net, (x, t, tauU) => tauU + 6 * x + 3 * t * t + 6 * t - Math.Pow(x, 3) - Math.Pow(t/2,3) - 0.625 , 0.5);

            new ParallelRunner(method)
                .WithTimer()
                .WithStandartComprarison((x, t) => Math.Pow(x, 3) + Math.Pow(t, 3) + 3 * Math.Pow(x, 2) + 3 * Math.Pow(t, 2), true)
                //.WithPrintStandart((x,t) => x * Math.Sin(t))
                //.WithDisplay()
                .Run();
        }


        public static void CalculateExampleThree(double h, double d)
        {
            var net = new Net(4, Math.PI, Math.PI, d, h)
                .SetDelayFunction((x, t) => x * Math.Sin(t))
                .SetBorderCondition(t => 0);

            var method = new IntegralRightAngleMethod(net, (x, t, tauU) => Math.Sin(t) - tauU, Math.PI, Math.PI/2);
            new ClassicRunner(method)
                .WithTimer()
                .WithStandartComprarison((x, t) => x * Math.Sin(t), true)
                //.WithPrintStandart((x,t) => x * Math.Sin(t))
                //.WithDisplay()
                .Run();
        }
    }
}
