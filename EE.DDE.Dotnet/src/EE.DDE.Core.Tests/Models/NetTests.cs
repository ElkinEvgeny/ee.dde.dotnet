﻿using System;
using EE.DDE.Core.Models;
using Xunit;

namespace EE.DDE.Core.Tests.Models
{
    public class NetTests
    {
        private const double TestValue = 666;

        [Fact]
        public void Constructor_MatrixSizeAssert()
        {
            var net = new Net(3, 4, 2, 1, 1);
            Assert.Equal(4, net.Height);
            Assert.Equal(3, net.Width);
            Assert.Equal(3, net.Offset);
        }

        [Theory]
        [InlineData(1, 1, 1)]
        [InlineData(1, 3, 3)]
        [InlineData(1, -2, -2)]
        [InlineData(0.5, 0, 0)]
        [InlineData(0.5, -2, -1)]
        public void ImageT_AssertImagesByIndex(double d, int j, double expectedImage)
        {
            var net = new Net(3, 4, 2, d, 1);
            Assert.Equal(expectedImage, net.ImageT(j));
        }

        [Theory]
        [InlineData(1, 1, 1)]
        [InlineData(1, 3, 3)]
        [InlineData(0.5, 0, 0)]
        [InlineData(0.5, 2, 1)]
        public void ImageX_AssertImagesByIndex(double h, int i, double expectedImage)
        {
            var net = new Net(3, 4, 2, 1, h);
            Assert.Equal(expectedImage, net.ImageX(i));
        }

        [Fact]
        public void SetBorderCondition_AssertZeroColumn()
        {
            var net = new Net(3, 4, 2, 1, 1);
            net.SetBorderCondition(x => x);
            for (var i = net.Height; i > -net.Offset; i--)
                Assert.Equal(net.ImageT(i), net[0,i]);
        }

        [Fact]
        public void SetDelayFunction_AssertRowInDelayPart()
        {
            var net = new Net(3, 4, 2, 1, 1);
            net.SetDelayFunction((x,y) => x);
            for (var row = 0; row > -net.Offset; row--)
                for (var i = 1; i <= net.Width; i++)
                    Assert.Equal(net.ImageX(i), net[i, row]);
        }

        [Fact]
        public void SetDelayFunction_AssertColumnInDelayPart()
        {
            var net = new Net(3, 4, 2, 1, 1);
            net.SetDelayFunction((x, y) => y);
            for (var column = 1; column <= net.Width; column++)
                for (var i = 0; i > -net.Offset; i--)
                    Assert.Equal(net.ImageT(i), net[column, i]);
        }
    }
}
